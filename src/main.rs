#![feature(proc_macro_hygiene, decl_macro)]

pub mod db;
pub mod backend_result;

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;
#[macro_use] extern crate serde_derive;
extern crate dotenv;

use rocket::Request;
use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use rocket_contrib::json::Json;
use db::models::*;
use db::connection::*;
use backend_result::BackendResult;
use backend_result::TemplateResult;

type Session<'a> = rocket_session::Session<'a, std::collections::HashMap<String, String>>;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/users")]
fn user_list(conn: DbConn) -> BackendResult<Vec<User>> {
    let list = User::list(conn)?;
    Ok(Json(list))
}

#[post("/users", data = "<user>")]
fn create_user<'a>(conn: DbConn, user: Json<NewUser>) -> BackendResult<User> {
    let user = user.into_inner();
    let user = user.insert(conn)?;
    Ok(Json(user))
}

#[get("/users/<name>")]
fn find_user(conn: DbConn, name: String) -> BackendResult<User> {
    let user = User::find_by_name(conn, &name)?;
    Ok(Json(user))
}

#[put("/users/<name>", data = "<user>")]
fn update_user_password(conn: DbConn, name: String, user: Json<NewUser>) -> BackendResult<User> {
    use backend_result::BackendError;

    let user = user.into_inner();

    if user.username != name {
        return Err(BackendError::Unauthorized);
    }

    let user = user.update(conn)?;
    Ok(Json(user))
}

#[delete("/users/<name>")]
fn delete_user(conn: DbConn, name: String) -> BackendResult<usize> {
    let deleted = User::delete(conn, &name)?;
    Ok(Json(deleted))
}

#[get("/login")]
fn login_page() -> Template {
    let map = std::collections::HashMap::<String, String>::new(); //TODO: static page?
    Template::render("login", &map)
}

#[post("/login", data = "<credentials>")]
fn login(conn: DbConn, session: Session, credentials: Json<NewUser>) -> BackendResult<String> {
    use backend_result::BackendError;

    let credentials = credentials.into_inner();

    let user = User::find_by_name(conn, &credentials.username)?;

    user.verify_password(credentials.password.as_bytes()).map_err(|e| {
        //TODO
        session.clear();
        BackendError::DbError(e)
    })?;

    session.tap(|map| {
        map.insert("username".to_string(), user.username.clone()); //TODO: check return? returns previous value or None
    });

    Ok(Json(format!("Signed in as {}", user.username)))
}

#[post("/logout")]
fn logout(session: Session) -> TemplateResult {
    use backend_result::BackendError;

    let optional_user = session.tap(|map| {
        map.get("username").map(|s| s.clone())
    });

    match optional_user {
        Some(_) => {
            session.clear();
            Ok(login_page())
        },
        None => Err(BackendError::Unauthorized)
    }
}

#[get("/home")]
fn home(session: Session) -> Result<Template, Redirect> {
    let optional_user = session.tap(|map| {
        map.get("username").map(|s| s.clone())
    });

    match optional_user {
        Some(user) => {
            let mut map = std::collections::HashMap::new();
            map.insert("username", user);
            Ok(Template::render("home", &map))
        },
        None => Err(Redirect::to(uri!(login)))
    }

}

#[get("/update_password")]
fn update_password_page(session: Session) -> Template {
    Template::render("update_password", session.tap(|map| {map.clone()})) //TODO: use std::convert::identity here?
}

fn show_error(error: &'static str, request: &Request) -> Template {
    use std::collections::HashMap;

    let mut map = HashMap::new();

    map.insert("error", error);

    map.insert("method", request.method().as_str());

    map.insert("path", request.uri().path());

    map.insert("query", match request.uri().query() {
        Some(query) => query,
        None => ""
    });

    let segment_count = request.uri().segment_count().to_string();
    map.insert("segment_count", &segment_count);

    let headers = format!("{:#?}", request.headers());
    map.insert("headers", &headers);

    let remote = match request.remote() {
        Some(value) => format!("{:?}", value),
        None => "".to_string()
    };
    map.insert("remote", &remote);

    Template::render("error", &map)
}

#[catch(400)]
fn bad_request(request: &Request) -> Template {
    show_error("400: Bad Request", request)
}

#[catch(404)]
fn not_found(request: &Request) -> Template {
    show_error("404: Page Not Found", request)
}

#[catch(422)]
fn unprocessable_entity_error(request: &Request) -> Template {
    show_error("422: Unprocessable Entity", request)
}

#[catch(500)]
fn internal_server_error(request: &Request) -> Template {
    show_error("500: Internal Server Error", request)
}

fn main() {
    use rocket_contrib::serve::StaticFiles;

    rocket::ignite()
        .attach(Template::fairing())
        .attach(DbConn::fairing())
        .attach(UserSessionsDbConn::fairing())
        .attach(Session::fairing().with_cookie_name("saa_session"))
        .mount("/", StaticFiles::from("static"))
        .mount("/", routes![
            index,
            user_list,
            create_user,
            find_user,
            update_user_password,
            delete_user,
            login_page,
            login,
            logout,
            home,
            update_password_page,
            ])
        .register(catchers![
            bad_request,
            not_found,
            internal_server_error,
            unprocessable_entity_error,
            ])
        .launch();
}