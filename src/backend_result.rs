use crate::db::db_result::*;
use rocket::response;
use rocket::response::Responder;
use rocket::Request;
use rocket_contrib::json::Json;
use rocket_contrib::templates::Template;

#[derive(Debug)]
pub enum BackendError {
    DbError(DbError),
    Unauthorized,
    Other(String),
}

impl From<DbError> for BackendError {
    fn from(error: DbError) -> Self {
        BackendError::DbError(error)
    }
}

impl<'a> Responder<'a> for BackendError {
    fn respond_to(self, _request: &Request) -> response::Result<'a> {
        use rocket::response::Response;
        use rocket::http::Status;

        let status = match self {
            BackendError::DbError(e) => match e {
                DbError::InvalidPassword | DbError::UsernameNotFound(_) => Status::Unauthorized,
                _ => Status::InternalServerError,
            }
            BackendError::Unauthorized => Status::Unauthorized,
            BackendError::Other(_) => Status::InternalServerError,
        };

        Response::build().status(status).ok()
    }
}

pub type BackendResult<T> = Result<Json<T>, BackendError>;
pub type TemplateResult = Result<Template, BackendError>;
